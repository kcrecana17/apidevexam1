<?php

use App\Http\Controllers\ExamController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return [
        'message' => 'Invalid URL: Please use /api thanks'
    ];
});

Route::get('data', [ExamController::class, 'get']);
Route::post('data', [ExamController::class, 'post']);
