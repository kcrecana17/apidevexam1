<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExamController extends Controller
{
    function get()
    {
        return [
            'code' => '400',
            'message' => 'Please use POST as request'
        ];
    }

    function post(Request $request)
    {
        $data = $request->all();

        $errors = [];
        if (!isset($data['process_code'])) {
            $errors[] = 'Invalid format: process code.';
        }

        if (!isset($data['request_data']['taken_written_exam']) || !is_bool($data['request_data']['taken_written_exam'])) {
            $errors[] = 'Invalid format: request_data taken_written_exam';
        }

        if (!isset($data['request_data']['taken_interview']) || !is_bool($data['request_data']['taken_interview'])) {
            $errors[] = 'Invalid format: request_data taken_interview';
        }

        if (!isset($data['request_data']['score']) || !is_numeric($data['request_data']['score'])) {
            $errors[] = 'Invalid format: request_data score';
        }

        if ($errors) {
            return [
                'code' => '400',
                'message' => $errors
            ];
        }

        $processCode = isset($data['process_code']) ? $data['process_code'] : '';

        $writtenExamValidation = ($data['request_data']['taken_written_exam']) ? 'I\'ve taken the Exam' : 'I will take the exam later';
        $interviewValidation = ($data['request_data']['taken_interview']) ? 'I\'ve taken the Interview' : 'I will take the interview later';
        $scoreValidation = ($data['request_data']['score'] >= 75) ? 'passed' : 'failed';

        $writtenExamAddition = ($data['request_data']['taken_written_exam']) ? 'Already taken the Exam' : 'Unable to take the Exam';
        $interviewAddition = ($data['request_data']['taken_interview']) ? 'Done with the Interview' : 'Unable to attend the Interview';

        return [
            'process_name' => 'Exam Process',
            'process_code' => $processCode,
            'process_flow' => [
                'ExamProcessManagerClass' => [
                    'extractData' => 'DefaultExtractorData',
                    'validateData' => [
                        'ExamDataValidatorClass' => [$writtenExamValidation, $interviewValidation, $scoreValidation],
                    ],
                    'additionalProcess' => [
                        'ExamAdditionalProcessClass' => [$writtenExamAddition, $interviewAddition]
                    ]
                ]
            ]
        ];
    }
}
